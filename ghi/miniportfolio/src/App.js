
import './App.css';

import Navbar from './components/Navbar.js'
import Coverpage from './components/Coverpage'
import Skills from './components/Skills'
import Contact from './components/Contact'
import Projects from './components/Projects';


function App() {
  return (
    <>
    <div>
      <Navbar />
      <Coverpage />
      <Projects />
      <Skills />
      <Contact />
    </div>
    </>
  );
}

export default App;

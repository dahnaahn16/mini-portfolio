import './Skills.css'
function Skills(){
    return(
        <div style={{marginTop:"200px"}}>
        <div className="h-100 d-flex align-items-center justify-content-center" > <h1>Skills</h1></div>
        <div className="h-100 d-flex align-items-center justify-content-center p-4">

            <button className="skills">
            <span class="button_top"><i className="devicon-python-plain">Python</i></span>
            </button>

            <button className="skills">
            <span class="button_top"><i class="devicon-docker-plain">Docker</i></span>
            </button>

            <button className="skills">
            <span class="button_top"><i className="devicon-react-original">React</i></span>
            </button>

            <button className="skills">
            <span class="button_top"><i class="devicon-javascript-plain">JavaScript</i></span>
            </button>

            <button className="skills">
            <span class="button_top"><i class="devicon-django-plain colored">Django</i></span>
            </button>

            <button className="skills">
            <span class="button_top"><i class="devicon-css3-plain">CSS</i></span>
            </button>

            <button className="skills">
            <span class="button_top"><i class="devicon-html5-plain">HTML5</i></span>
            </button>

        </div>
        </div>
    )
}


export default Skills

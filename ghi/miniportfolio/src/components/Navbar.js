
function Navbar(){
return (
<nav className="navbar navbar-light" style={{ backgroundColor: "#e3f2fd", padding:"40px" }}>
    <div className="container-fluid">

    <a className="navbar-brand"> Dahna Ahn </a>

    <ul className="navbar-nav ml-auto mt-2 mt-lg-0 flex-row justify-content-end">

        <li className="nav-item">
            <a className="navbar-brand mb-0" href="#">About me</a>
        </li>

        <li className="nav-item">
            <a className="navbar-brand mb-0" href="#">Projects</a>
        </li>

        <li className="nav-item">
            <a className="navbar-brand mb-0" href="#">Skills</a>
        </li>

        <li className="nav-item">
            <button type="button" className="btn btn-outline-dark btn-sm" >Let's chat!</button>
        </li>

    </ul>
    </div>
</nav>
    )
}

export default Navbar;

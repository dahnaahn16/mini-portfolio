import './Coverpage.css'
import me from "./images/me.png"

function Coverpage(){
    return(
    <>
    <div style={{padding:"200px"}}>
    <img className = "me" src={me} alt="me" />
    <div className="intro">
        <h1>Hello I'm Dahna!</h1>
        <br />
        <p>Software Engineer from San Jose, California</p>
    </div>
    </div>

    </>
    )
}

export default Coverpage
